//
// Created by Vasyl Ahosta on 6/13/17.
// Copyright (c) 2017 Vasyl Ahosta. All rights reserved.
//

import XCTest
import SwiftEvents

final class ObservableTests: TestCase {

    private var token: EventSubscriptionToken?
    private var secondaryToken: EventSubscriptionToken?
    private var equitableProperty = Observable<String>(value: "The initial value")
    private var optionalProperty = Observable<Int?>(value: 0)
    private var notEquitableProperty = Observable<MockObject>(value: MockObject())

    override func setUp() {
        super.setUp()
        equitableProperty = Observable<String>(value: "The initial value")
        optionalProperty = Observable<Int?>(value: 0)
        notEquitableProperty = Observable<MockObject>(value: MockObject())
    }

    func testObservableShouldStoreInitialValue() {
        let aVariable = Observable<Int>(value: 0)
        XCTAssertEqual(aVariable.value, 0)
    }

    func testObservableShouldStoreInitialOptionalValue() {
        let aVariable = Observable<String?>(value: nil)
        XCTAssertNil(aVariable.value)
    }

    func testObservableShouldStoreNewValue() {
        let newValue = 1234
        optionalProperty =~ newValue
        XCTAssertEqual(optionalProperty.value, newValue)
        optionalProperty =~ nil
        XCTAssertNil(optionalProperty.value)
    }

    func testDidSetValueEvent_ShouldBeRaised() {
        assertRaised(equitableProperty.didSetValueEvent, updater: { equitableProperty =~ "Test Value" })
        assertRaised(optionalProperty.didSetValueEvent, updater: { optionalProperty =~ 1234 })
        assertRaised(notEquitableProperty.didSetValueEvent, updater: { notEquitableProperty =~ MockObject() })
    }


    func testDidSetValueEvent_ShouldProvideOldAndNewValueInUserInfo() {
        assertUserInfoHasNewAndOldValues(equitableProperty.didSetValueEvent, of: equitableProperty, newValue: "Test Value")
        assertUserInfoHasNewAndOldValues(optionalProperty.didSetValueEvent, of: optionalProperty, newValue: 1234)
        assertUserInfoHasNewAndOldValues(notEquitableProperty.didSetValueEvent, of: notEquitableProperty, newValue: MockObject())
    }


    func testDidSetValueEvent_ShouldBeRaisedAfterValueIsChanged() {
        assertRaisedAfterValueIsChanged(equitableProperty.didSetValueEvent, of: equitableProperty, newValue: "Test Value")
        assertRaisedAfterValueIsChanged(optionalProperty.didSetValueEvent, of: optionalProperty, newValue: 1233)
        assertRaisedAfterValueIsChanged(notEquitableProperty.didSetValueEvent, of: notEquitableProperty, newValue: MockObject())
    }

    func testWillSetValueEvent_ShouldBeRaised() {
        assertRaised(equitableProperty.willSetValueEvent, updater: { equitableProperty =~ "Test Value" })
        assertRaised(optionalProperty.willSetValueEvent, updater: { optionalProperty =~ 1234 })
        assertRaised(notEquitableProperty.willSetValueEvent, updater: { notEquitableProperty =~ MockObject() })
    }

    func testWillSetValueEvent_ShouldProvideOldAndNewValueInUserInfo() {
        assertUserInfoHasNewAndOldValues(equitableProperty.willSetValueEvent, of: equitableProperty, newValue: "Test Value")
        assertUserInfoHasNewAndOldValues(optionalProperty.willSetValueEvent, of: optionalProperty, newValue: 1234)
        assertUserInfoHasNewAndOldValues(notEquitableProperty.willSetValueEvent, of: notEquitableProperty, newValue: MockObject())
    }

    func testWillSetValueEvent_ShouldBeRaisedBeforeValueIsChanged() {
        assertRaisedBeforeValueIsChanged(equitableProperty.willSetValueEvent, of: equitableProperty, newValue: "Test Value")
        assertRaisedBeforeValueIsChanged(optionalProperty.willSetValueEvent, of: optionalProperty, newValue: 1234)
        assertRaisedBeforeValueIsChanged(notEquitableProperty.willSetValueEvent, of: notEquitableProperty, newValue: MockObject())
    }

    func testDidChangeValueEvent_ShouldBeRaisedWhenNewValueIsNotEqualToOldOne() {
        assertRaised(equitableProperty.didChangeValueEvent, updater: { equitableProperty =~ "Test Value" })
        assertRaised(optionalProperty.didChangeValueEvent, updater: { optionalProperty =~ 1234 })
    }

    func testDidChangeValueEvent_ShouldNotBeRaisedWhenNewValueIsEqualToOldOne() {
        let value = equitableProperty.value
        _ = assertNotRaised(equitableProperty.didChangeValueEvent) { equitableProperty =~ value }
        let number = 123
        optionalProperty =~ number
        let token = assertNotRaised(optionalProperty.didChangeValueEvent) { optionalProperty =~ number}
        token.unsubscribe()
        optionalProperty =~ nil
        _ = assertNotRaised(optionalProperty.didChangeValueEvent) { optionalProperty =~ nil}
    }

    func testDidChangeValueEvent_ShouldProvideNewAndOldValueInUserInfo() {
        assertUserInfoHasNewAndOldValues(equitableProperty.didChangeValueEvent, of: equitableProperty, newValue: "Test Value")
        assertUserInfoHasNewAndOldValues(optionalProperty.didChangeValueEvent, of: optionalProperty, newValue: 123445)
    }

    func testDidChangeValueEvent_ShouldBeRaisedAfterValueIsChanged() {
        assertRaisedAfterValueIsChanged(equitableProperty.didChangeValueEvent, of: equitableProperty, newValue: "Test Value")
        assertRaisedAfterValueIsChanged(optionalProperty.didChangeValueEvent, of: optionalProperty, newValue: 1233)
    }

    func testWillChangeValueEvent_ShouldBeRaisedWhenNewValueIsNotEqualToOldOne() {
        assertRaised(equitableProperty.willChangeValueEvent, updater: { equitableProperty =~ "Test Value" })
        assertRaised(optionalProperty.willChangeValueEvent, updater: { optionalProperty =~ 1234 })
    }

    func testWillChangeValueEvent_ShouldNotBeRaisedWhenNewValueIsEqualToOldOne() {
        let value = equitableProperty.value
        _ = assertNotRaised(equitableProperty.willChangeValueEvent) { equitableProperty =~ value }
        let number = 123
        optionalProperty =~ number
        let token = assertNotRaised(optionalProperty.willChangeValueEvent) { optionalProperty =~ number}
        token.unsubscribe()
        optionalProperty =~ nil
        _ = assertNotRaised(optionalProperty.willChangeValueEvent) { optionalProperty =~ nil}
    }

    func testWillChangeValueEvent_ShouldProvideNewValueInUserInfo() {
        assertUserInfoHasNewAndOldValues(equitableProperty.willChangeValueEvent, of: equitableProperty, newValue: "Test Value")
        assertUserInfoHasNewAndOldValues(optionalProperty.willChangeValueEvent, of: optionalProperty, newValue: 123445)
    }

    func testWillChangeValueEvent_ShouldBeRaisedBeforeValueIsChanged() {
        assertRaisedBeforeValueIsChanged(equitableProperty.willChangeValueEvent, of: equitableProperty, newValue: "Test Value")
        assertRaisedBeforeValueIsChanged(optionalProperty.willChangeValueEvent, of: optionalProperty, newValue: 1234)
    }

    func testOnChangeValueEvent_ShouldBeRaised() {
        assertRaised(equitableProperty.onChangeValueEvent, updater: { equitableProperty =~ "Test Value" })
        assertRaised(optionalProperty.onChangeValueEvent, updater: { optionalProperty =~ 1234 })
    }

    func testOnChangeValueEvent_ShouldNotBeRaisedForWhenNewValueIsEqualToOldOne() {
        let value = equitableProperty.value
        _ = assertNotRaised(equitableProperty.onChangeValueEvent) { equitableProperty =~ value }
        let number = 123
        optionalProperty =~ number
        let token = assertNotRaised(optionalProperty.onChangeValueEvent) { optionalProperty =~ number}
        token.unsubscribe()
        optionalProperty =~ nil
        _ = assertNotRaised(optionalProperty.onChangeValueEvent) { optionalProperty =~ nil}
    }

    func testOnChangeValueEvent_ShouldProvideNewValueInUserInfo() {
        assertUserInfoHasNewAndOldValues(equitableProperty.onChangeValueEvent, of: equitableProperty, newValue: "Test Value")
        assertUserInfoHasNewAndOldValues(optionalProperty.onChangeValueEvent, of: optionalProperty, newValue: 123445)
    }

    func testOnChangeValueEvent_ShouldBeRaisedAfterValueIsChanged() {
        assertRaisedAfterValueIsChanged(equitableProperty.onChangeValueEvent, of: equitableProperty, newValue: "Test Value")
        assertRaisedAfterValueIsChanged(optionalProperty.onChangeValueEvent, of: optionalProperty, newValue: 1233)
    }

    func testOnChangeValueEvent_ShouldBeRaisedBeforeDidChangeValueEvent() {
        assert(equitableProperty.onChangeValueEvent, isRaisedBefore: equitableProperty.didChangeValueEvent) {
            equitableProperty =~ "The New Value"
        }
        assert(optionalProperty.onChangeValueEvent, isRaisedBefore: optionalProperty.didChangeValueEvent) {
            optionalProperty =~ 1234
        }
    }

    func testOnChangeEvent_ShouldBeRaisedAfterSubscription() {
        let value = 1345
        let aProperty = Observable<Int>(value: value)
        assertRaisedOnSubscribe(aProperty.willChangeValueEvent, andReturnsValue: value)
        assertRaisedOnSubscribe(aProperty.willSetValueEvent, andReturnsValue: value)
        assertRaisedOnSubscribe(aProperty.didSetValueEvent, andReturnsValue: value)
        assertRaisedOnSubscribe(aProperty.onChangeValueEvent, andReturnsValue: value)
        assertRaisedOnSubscribe(aProperty.didChangeValueEvent, andReturnsValue: value)
    }

    func testWhenSubscribedSkippingFirstThenShouldNoRaiseOnSubscribe() {
        let aProperty = Observable<Int>(value: 0)
        token = aProperty.didSetValueEvent.subscribe(skipFirst: true) { _ in
            XCTFail("Should no raise on subscribe when set \"skipFist\"")
        }
    }

    func testDeinitialization() {
        let eventBag = EventSubscriptionTokenBag()
        var aProperty: Observable<Int>? = Observable(value: 123)
        eventBag.add(aProperty!.willSetValueEvent += { _ in })
        eventBag.add(aProperty!.didSetValueEvent += { _ in })
        eventBag.add(aProperty!.willChangeValueEvent += { _ in })
        eventBag.add(aProperty!.onChangeValueEvent += { _ in })
        eventBag.add(aProperty!.didChangeValueEvent += { _ in })
        weak var weakProperty = aProperty
        aProperty = nil
        XCTAssertNil(weakProperty)
    }

    func testWhenBoundEventWithChangeThenObjectsPropertyShouldBeSetNewValue() {
        let object = TestObject()
        let observable = Observable(value: "")
        token = observable.didChangeValueEvent.bind(\.stringValue, of: object)
        let newValue = "Neue"
        observable =~ newValue
        XCTAssertEqual(newValue, object.stringValue)
    }

    func testGivenBoundAnObservableToAnEventWhenEventRaisedThenUpdateValueInTheObservable() {
        let observable = Observable(value: "")
        let event = Event<String>()
        token = observable.bind(to: event)
        let newValue = "New Value"
        event.raise(newValue)
        XCTAssertEqual(newValue, observable.value)
    }

    func testGivenBoundAnObservableToAnEventWhenEventRaisedThenTheObservableShouldRaiseWillSetAndOnChangeAndDidSetEvents() {
        let observable = Observable(value: 0)
        let event = Event<Int>()
        token = observable.bind(to: event)
        assertRaisedAllEvents(of: observable) { event.raise(1) }
    }

    func testGivenBoundAnObservableToAnEventWhenEventRaisedSeveralTimesThenTheObservableShouldRaiseDidChangeTheSameNumberOfTimes() {
        let observable = Observable(value: 0)
        let event = Event<Int>()
        var tokens: [EventSubscriptionToken] = []
        tokens.append(observable.bind(to: event))
        var actualRaiseCount = 0
        tokens.append(observable.didChangeValueEvent.subscribe(skipFirst: true, { _ in  actualRaiseCount += 1 }))
        let expectedRaiseCoint = 5
        for i in 1...expectedRaiseCoint {
            event.raise(i)
        }
        XCTAssertEqual(expectedRaiseCoint, actualRaiseCount)
    }

    func testBindingObservableToEventWithMap_MapShouldTransformValueFromEventTypeToObservableType() {
        let observable = Observable(value: "")
        let event = Event<Int>()
        token = observable.bind(to: event, map: { String(describing: $0) })
        let value = 1
        event.raise(value)
        XCTAssertEqual(String(describing: value), observable.value)
    }

    func testBindingObservableToEventWithMap_ShouldRaiseEvents() {
        let observable = Observable(value: "")
        let event = Event<Int>()
        token = observable.bind(to: event, map: { String(describing: $0) })
        assertRaisedAllEvents(of: observable) { event.raise(1) }
    }

    func testBindToEventWithChange_ShouldRaiseSetEvents() {
        let observable = Observable(value: TestObject())
        let event = Event<Observable<TestObject>.Change<TestObject>>()
        token = observable.bind(to: event)
        assertRaisedSetEvents(of: observable) {
            event.raise(Observable<TestObject>.Change(oldValue: TestObject(), newValue: TestObject()))
        }
    }

    func testBindToEventWithChangWithEquatableValue_ShouldRaiseAllEvents() {
        let observable = Observable(value: 0)
        let event = Event<Observable<Int>.Change<Int>>()
        token = observable.bind(to: event)
        assertRaisedAllEvents(of: observable) {
            event.raise(Observable<Int>.Change(oldValue: 0, newValue: 1))
        }
    }

    func testBindingObservableToEventWithChangeAndMap_MapShouldTransformValueFromEventTypeToObservableType() {
        let observable = Observable(value: "")
        let event = Event<Observable<Int>.Change<Int>>()
        token = observable.bind(to: event, map: { String(describing: $0.newValue) })
        let value = 1
        event.raise(Change(oldValue: 0, newValue: value))
        XCTAssertEqual(String(describing: value), observable.value)
    }

    func testBindToEventWithChangWithMap_ShouldRaiseAllEvents() {
        let observable = Observable(value: "")
        let event = Event<Observable<Int>.Change<Int>>()
        token = observable.bind(to: event) { String(describing: $0.newValue) }
        assertRaisedAllEvents(of: observable) {
            event.raise(Observable<Int>.Change(oldValue: 0, newValue: 1))
        }
    }

    func testChangeShouldBeEquitableForEquitableGenericParameter() {
        let change0 = Observable<Int>.Change<Int>(oldValue: 0, newValue: 0)
        let change1 = Observable<Int>.Change<Int>(oldValue: 1, newValue: 0)
        let change2 = Observable<Int>.Change<Int>(oldValue: 0, newValue: 1)
        XCTAssertEqual(change0, change0)
        XCTAssertNotEqual(change0, change1)
        XCTAssertNotEqual(change0, change2)
    }

    func testObservableShouldBeEquitableForEquitableGenericParameter() {
        let observable0 = Observable(value: 0)
        let observable1 = Observable(value: 1)
        XCTAssertEqual(observable0, observable0)
        XCTAssertNotEqual(observable0, observable1)
    }

    func testWhenBoundToEventSkippingFirstThenShouldNotSetValueForFirstTime() {
        let event = Event<Any>()
        let observable = Observable<Any>(value: "")
        token = observable.bind(to: event, skipFirst: true)
        let newValue = "NewValue" 
        event.raise(newValue)
        XCTAssertTrue((observable.value as! String).isEmpty, "Should not set value")
        event.raise(newValue)
        XCTAssertEqual(newValue, observable.value as! String)
    }

    func testWhenBoundToEventWithMapSkippingFirstThenShouldNotSetValueForFirstTime() {
        let event = Event<Int>()
        let observable = Observable<Any>(value: "")
        token = observable.bind(to: event, skipFirst: true) { "\($0)" }
        event.raise(1)
        XCTAssertTrue((observable.value as! String).isEmpty, "Should not set value")
        event.raise(1)
        XCTAssertEqual("1", observable.value as! String)
    }

    func testWhenBoundToEventWithChangeSkippingFirstThenShouldNotSetValueForFirstTime() {
        let event = Event<Observable<Any>.Change<Any>>()
        let observable = Observable<Any>(value: "")
        token = observable.bind(to: event, skipFirst: true)
        let newValue = Observable<Any>.Change<Any>(oldValue: nil, newValue: "NewValue")
        event.raise(newValue)
        XCTAssertTrue((observable.value as! String).isEmpty, "Should not set value")
        event.raise(newValue)
        XCTAssertEqual(newValue.newValue as! String, observable.value as! String)
    }

}

extension ObservableTests {

    typealias Change<T> = Observable<T>.Change<T>

    func assertRaised<T>(_ event: Event<Change<T>>, updater: () -> (), callback: ((Change<T>) -> Void)? = nil) {
        var didRaiseEvent = false
        token = event.subscribe(skipFirst: true) { userInfo in
            didRaiseEvent = true
            callback?(userInfo)
        }
        updater()
        XCTAssertTrue(didRaiseEvent)
    }

    func assertNotRaised<T>(_ event: Event<Change<T>>, updater: () -> ()) -> EventSubscriptionToken {
        let token = event.subscribe(skipFirst: true) { userInfo in
            XCTFail()
        }
        updater()
        return token
    }

    func assertUserInfoHasNewAndOldValues<T: Equatable>(_ event: Event<Change<T>>, of aProperty: Observable<T>, newValue: T) {
        let anOldValue = aProperty.value
        token = event.subscribe(skipFirst: true) { change in
            XCTAssertEqual(anOldValue, change.oldValue)
            XCTAssertEqual(newValue, change.newValue)
        }
        aProperty =~ newValue
    }

    func assertUserInfoHasNewAndOldValues<T: Equatable>(_ event: Event<Change<T?>>, of aProperty: Observable<T?>, newValue: T?) {
        let anOldValue = aProperty.value
        token = event.subscribe(skipFirst: true) { change in
            XCTAssertEqual(anOldValue, change.oldValue)
            XCTAssertEqual(newValue, change.newValue)
        }
        aProperty =~ newValue
    }

    func assertUserInfoHasNewAndOldValues<T: AnyObject>(_ event: Event<Change<T>>, of aProperty: Observable<T>, newValue: T) {
        let anOldValue = aProperty.value
        token = event.subscribe(skipFirst: true) { change in
            XCTAssertTrue(anOldValue === change.oldValue)
            XCTAssertTrue(newValue === change.newValue)
        }
        aProperty =~ newValue
    }

    func assertRaisedAfterValueIsChanged<T: Equatable>(_ event: Event<Change<T>>, of aProperty: Observable<T>, newValue: T) {
        XCTAssertNotEqual(newValue, aProperty.value)
        assertRaised(event, updater: { aProperty =~ newValue }) { XCTAssertEqual($0.newValue, aProperty.value) }
    }

    func assertRaisedAfterValueIsChanged<T: Equatable>(_ event: Event<Change<T?>>, of aProperty: Observable<T?>, newValue: T?) {
        XCTAssertNotEqual(newValue, aProperty.value)
        assertRaised(event, updater: { aProperty =~ newValue }) { XCTAssertEqual($0.newValue, aProperty.value) }
    }

    func assertRaisedAfterValueIsChanged<T: AnyObject>(_ event: Event<Change<T>>, of aProperty: Observable<T>, newValue: T) {
        XCTAssertFalse(newValue === aProperty.value)
        assertRaised(event, updater: { aProperty =~ newValue }) { XCTAssertTrue($0.newValue === aProperty.value) }
    }

    func assertRaisedBeforeValueIsChanged<T: Equatable>(_ event: Event<Change<T>>, of aProperty: Observable<T>, newValue: T) {
        assertRaised(event, updater: { aProperty =~ newValue }) { XCTAssertNotEqual($0.newValue, aProperty.value) }
    }

    func assertRaisedBeforeValueIsChanged<T: Equatable>(_ event: Event<Change<T?>>, of aProperty: Observable<T?>, newValue: T?) {
        assertRaised(event, updater: { aProperty =~ newValue }) { XCTAssertNotEqual($0.newValue, aProperty.value) }
    }

    func assertRaisedBeforeValueIsChanged<T: AnyObject>(_ event: Event<Change<T>>, of aProperty: Observable<T>, newValue: T) {
        assertRaised(event, updater: { aProperty =~ newValue }) { XCTAssertFalse($0.newValue === aProperty.value) }
    }

    func assert<T>(_ firstEvent: Event<Change<T>>, isRaisedBefore secondEvent: Event<Change<T>>, trigger: () -> ()) {
        var didRaiseFirstEvent = false
        var didRaiseSecondEvent = false
        token = firstEvent.subscribe(skipFirst: true) { _ in
            didRaiseFirstEvent = true
            XCTAssertFalse(didRaiseSecondEvent)
        }
        secondaryToken = secondEvent.subscribe(skipFirst: true) { _ in
            XCTAssertTrue(didRaiseFirstEvent)
            didRaiseSecondEvent = true
        }
        trigger()
        XCTAssertTrue(didRaiseSecondEvent)
    }

    func assertRaisedOnSubscribe<T: Equatable>(_ event: Event<Change<T>>, andReturnsValue value: T) {
        var eventWasRaised = false
        token = event += { change in
            eventWasRaised = true
            XCTAssertNil(change.oldValue)
            XCTAssertEqual(change.newValue, value)
        }
        XCTAssertTrue(eventWasRaised)
    }

    private func assertRaisedSetEvents<T>(of observable: Observable<T>, on action: () -> Void) {
        var tokens: [EventSubscriptionToken] = []
        var eventLogs: String = ""
        tokens.append(observable.willSetValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->willSet" }))
        tokens.append(observable.didSetValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->didSet" }))
        action()
        XCTAssertEqual("->willSet->didSet", eventLogs)
    }

    func assertRaisedAllEvents<T: Equatable>(of observable: Observable<T>, on action: () -> Void) {
        var tokens: [EventSubscriptionToken] = []
        var eventLogs: String = ""
        tokens.append(observable.willChangeValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->willChange" }))
        tokens.append(observable.willSetValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->willSet" }))
        tokens.append(observable.didSetValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->didSet" }))
        tokens.append(observable.onChangeValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->onChange" }))
        tokens.append(observable.didChangeValueEvent.subscribe(skipFirst: true, { _ in  eventLogs += "->didChange" }))
        action()
        XCTAssertEqual("->willChange->willSet->didSet->onChange->didChange", eventLogs)
    }

}
