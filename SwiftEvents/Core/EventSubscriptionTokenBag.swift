//
// Created by Vasyl Ahosta on 2019-01-06.
// Copyright (c) 2019 Vasyl Ahosta. All rights reserved.
//

import Foundation

public class EventSubscriptionTokenBag {

    private var tokens: [EventSubscriptionToken] = []

    public init() {}

    public func add(_ token: EventSubscriptionToken) {
        tokens.append(token)
    }

    public func clear() {
        tokens = []
    }

}
