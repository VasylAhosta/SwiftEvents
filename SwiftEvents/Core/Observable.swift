//
// Created by Vasyl Ahosta on 6/13/17.
// Copyright (c) 2017 Vasyl Ahosta. All rights reserved.
//

import Foundation

public class Observable<T> {

    public struct Change<T> {
        public let oldValue: T?
        public let newValue: T

        public init(oldValue: T?, newValue: T) {
            self.oldValue = oldValue
            self.newValue = newValue
        }
    }

    public fileprivate(set) var value: T {
        willSet { willSetValueEvent.raise(Change(oldValue: value, newValue: newValue)) }
        didSet { didSetValueEvent.raise(Change(oldValue: oldValue, newValue: value)) }
    }

    public let willSetValueEvent = Event<Change<T>>()
    public let didSetValueEvent = Event<Change<T>>()

    @available(*, deprecated, message: "Please subscribe on event with 'skipFirst' option instead")
    public var raiseOnSubscribe: Bool = true

    private let _willChangeValueEvent = Event<Change<T>>()
    private let _onChangeValueEvent = Event<Change<T>>()
    private let _didChangeValueEvent = Event<Change<T>>()

    public init(value: T) {
        self.value = value
        willSetValueEvent.delegate = self
        didSetValueEvent.delegate = self
        _willChangeValueEvent.delegate = self
        _onChangeValueEvent.delegate = self
        _didChangeValueEvent.delegate = self
    }

    public func bind(to event: Event<T>, skipFirst: Bool = false) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] in self?.value = $0 }
    }

    public func bind<E>(to event: Event<E>, skipFirst: Bool = false, map: @escaping (E) -> T) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] in self?.value = map($0) }
    }

    public func bind(to event: Event<Change<T>>, skipFirst: Bool = false) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] in self?.value = $0.newValue }
    }

}

extension Observable: EventDelegate {

    internal func event<E>(_ event: Event<E>, didAddHandler handlerWrapper: Event<E>.HandlerWrapper) {
        if raiseOnSubscribe {
            handlerWrapper.raise(Change(oldValue: nil, newValue: value) as! E)
        }
    }

}

extension Observable where T: Equatable {

    public var willChangeValueEvent: Event<Change<T>> {
        return _willChangeValueEvent
    }

    public var onChangeValueEvent: Event<Change<T>> {
        return _onChangeValueEvent
    }

    public var didChangeValueEvent: Event<Change<T>> {
        return _didChangeValueEvent
    }

    fileprivate func set(_ newValue: T) {
        let isNotEqual = newValue != value
        let oldValue = value
        if isNotEqual {
            willChangeValueEvent.raise(Change(oldValue: oldValue, newValue: newValue))
        }
        value = newValue
        if isNotEqual {
            onChangeValueEvent.raise(Change(oldValue: oldValue, newValue: newValue))
            didChangeValueEvent.raise(Change(oldValue: oldValue, newValue: newValue))
        }
    }

    public func bind(to event: Event<T>, skipFirst: Bool = false) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] in  self?.set($0) }
    }

    public func bind<E>(to event: Event<E>, skipFirst: Bool = false, map: @escaping (E) -> T) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] in self?.set(map($0)) }
    }

    public func bind(to event: Event<Change<T>>, skipFirst: Bool = false) -> EventSubscriptionToken {
        return event.subscribe(skipFirst: skipFirst) { [weak self] in self?.set($0.newValue) }
    }

}

infix operator =~ : AssignmentPrecedence
public func =~<T>(observable: Observable<T>, newValue: T) {
    observable.value = newValue
}

public func =~<T: Equatable>(observable: Observable<T>, newValue: T) {
    observable.set(newValue)
}

extension Event {

    public func bind<Target: AnyObject, V>(_ keyPath: WritableKeyPath<Target, V>, of target: Target, skipFirst: Bool = false) -> EventSubscriptionToken where T == Observable<V>.Change<V> {
        return bind(keyPath, of: target, skipFirst: skipFirst) { $0.newValue }
    }

}

extension Observable: Equatable where T: Equatable {

    public static func ==(lhs: Observable<T>, rhs: Observable<T>) -> Bool {
        return lhs.value == rhs.value
    }

}

extension Observable.Change: Equatable where T: Equatable {}
