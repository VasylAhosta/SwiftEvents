//
// Created by Vasyl Ahosta on 5/9/18.
// Copyright (c) 2018 Vasyl Ahosta. All rights reserved.
//

import Foundation

public protocol EventSubscriber: class {

    var eventSubscriptionTokens: [EventSubscriptionToken] { get set }

}

extension EventSubscriber {

    public func subscribe<T>(for event: Event<T>, handler: @escaping Event<T>.Handler) {
        let token = event += handler
        eventSubscriptionTokens.append(token)
    }

    public func bind<Target: AnyObject, V>(_ target: Target, _ keyPath: WritableKeyPath<Target, V>, to event: Event<V>) {
        let token = event.bind(keyPath, of: target)
        eventSubscriptionTokens.append(token)
    }

    public func bind<Target: AnyObject, V, T>(_ target: Target, _ keyPath: WritableKeyPath<Target, V>, to event: Event<T>, map: @escaping (T) -> V) {
        let token = event.bind(keyPath, of: target, map: map)
        eventSubscriptionTokens.append(token)
    }

    public func bind<Target: AnyObject, V>(_ target: Target, _ keyPath: WritableKeyPath<Target, V>, to event: Event<Observable<V>.Change<V>>) {
        let token = event.bind(keyPath, of: target)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T>(_ targetEvent: Event<T>, to sourceEvent: Event<T>) {
        let token = targetEvent.bind(to: sourceEvent)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T>(_ observable: Observable<T>, to event: Event<T>) {
        let token = observable.bind(to: event)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T>(_ observable: Observable<T>, to event: Event<Observable<T>.Change<T>>) {
        let token = observable.bind(to: event)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T: Equatable>(_ observable: Observable<T>, to event: Event<T>) {
        let token = observable.bind(to: event)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T: Equatable>(_ observable: Observable<T>, to event: Event<Observable<T>.Change<T>>) {
        let token = observable.bind(to: event)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T, E>(_ observable: Observable<T>, to event: Event<E>, map: @escaping (E) -> T) {
        let token = observable.bind(to: event, map: map)
        eventSubscriptionTokens.append(token)
    }

    public func bind<T: Equatable, E: Equatable>(_ observable: Observable<T>, to event: Event<E>, map: @escaping (E) -> T) {
        let token = observable.bind(to: event, map: map)
        eventSubscriptionTokens.append(token)
    }

    public func unsubscribeFromAllEvents() {
        eventSubscriptionTokens = []
    }

}
